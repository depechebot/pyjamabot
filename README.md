
Install it as easy as:

```
$ mkdir stable
$ cd stable
$ go install .. # may take several minutes to build libraries for first time
$ pyjamabot
```

Also one need to read README.md at nlp/ folder to make nlp-specific installation as well.