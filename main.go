package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/bluele/slack"
	"github.com/imdario/mergo"

	statjson "bitbucket.org/depechebot/stat/model/json"
	dbot "github.com/depechebot/depechebot"
	dbotsql "github.com/depechebot/depechebot/model/sqlite"

	_ "github.com/mattn/go-sqlite3"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const (
	botName       = "Pyjama bot"
	telegramToken = "562244526:AAF8twSlJ2dsiE-xSXoMMU5SkEP7JougbG0"
	dbName        = "pyjamabot.sqlite3"
	statFilename  = "pyjamabot.stat"

	slackToken   = "xoxb-78291074068-wCA6ITkz9PnMoiG2cxfpkHkz"
	slackChannel = "telegram-messages"
)

const (
	logChatID = -1001337430955
)

var slackbot *slack.Slack

// chatLog should be thread-safe
func chatLog(bot dbot.Bot, update tgbotapi.Update, chat dbot.Chat) {
	var s string
	var s2 string
	if update.Message.From.UserName != "" {
		s = fmt.Sprintf("[%s]. %s %s (@%s): %s", botName, update.Message.From.FirstName,
			update.Message.From.LastName, update.Message.From.UserName, update.Message.Text)
		if chat.Params.Get("adminLog") == "yes" {
			s2 = fmt.Sprintf("`%s %s (@%s): %s`", update.Message.From.FirstName,
				update.Message.From.LastName, update.Message.From.UserName, update.Message.Text)
		} else {
			s2 = fmt.Sprintf("`%s %s (`@%s`): %s`", update.Message.From.FirstName,
				update.Message.From.LastName,
				strings.Replace(update.Message.From.UserName, "_", "\\_", -1),
				update.Message.Text)
		}
	} else {
		s = fmt.Sprintf("[%s]. %s %s: %s", botName, update.Message.From.FirstName,
			update.Message.From.LastName, update.Message.Text)
		s2 = fmt.Sprintf("`%s %s: %s`", update.Message.From.FirstName,
			update.Message.From.LastName, update.Message.Text)
	}

	log.Print(s)

	// todo: is it thread-safe?
	err := slackbot.ChatPostMessage(slackChannel, s, &slack.ChatPostMessageOpt{AsUser: true})
	if err != nil {
		log.Printf("Error: %v", err)
	}

	_ = s2
	// send log string to admin log chat
	if chat.ChatID > 0 {
		msg := tgbotapi.NewMessage(int64(logChatID), s2)
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.SendChan <- dbot.ChatSignal{msg, dbot.ChatID(logChatID)}
	}

}

var statModel statjson.Model

func main() {

	slackbot = slack.New(slackToken)

	logFile, err := os.OpenFile("log", os.O_CREATE|os.O_RDWR|os.O_APPEND, 0660)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	dbotdb, err := sql.Open("sqlite3", dbName)
	if err != nil {
		log.Panic(err)
	}
	defer func() {
		if e := dbotdb.Close(); err == nil {
			err = e
		}
	}()

	log.Println("Database file used:", dbName)
	model := dbotsql.NewModel(dbotdb)

	log.Println("Stat file used:", statFilename)
	statModel = statjson.NewModel(statFilename)
	err = statModel.Init()
	if err != nil {
		log.Panic(err)
	}

	commonLog := func(update tgbotapi.Update) {
		fmt.Fprint(logFile, marshal(update), "\n")
		statModel.Update(&update)
	}
	err = mergo.Merge(&statesConfigPrivate, adminStatesConfigPrivate)
	if err != nil {
		log.Panic(err)
	}

	config := dbot.Config{
		TelegramToken:       telegramToken,
		CommonLog:           commonLog,
		ChatLog:             chatLog,
		StatesConfigPrivate: statesConfigPrivate,
		StatesConfigGroup:   statesConfigGroup,
		Model:               model,
	}

	bot, err := dbot.New(config)
	if err != nil {
		log.Panic(err)
	}

	bot.Run()
	log.Println("Reached the end??")
}

func marshal(data interface{}) string {
	out, err := json.Marshal(data)
	if err != nil {
		log.Panic(err)
	}
	return string(out)
}
