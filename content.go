package main

import dbot "github.com/depechebot/depechebot"

var (
	chooseMenuText        = dbot.NewText("Choose a menu item:")
	underConstructionText = dbot.NewText("Under construction")

	announceText = dbot.NewTextWithMarkdown(`
Hey! I'm PyjamaBot (aka AdsBot), and I'll analyze your data like a God.
`)

	aboutText = dbot.NewText(`
Hey! I'm PyjamaBot (aka AdsBot), and I'll analyze your data like a God.
`)

	uploadText  = dbot.NewText("Upload your data (excel) or choose one of ready-made datasets:")
	requestText = dbot.NewText("Make a query to your data:")

	helpText = dbot.NewText("Here you can ask your question and find an answer")
)

var (
	infoRequest       = dbot.NewRequest("Info")
	usePreloadRequest = dbot.NewRequest("Random query")

	uploadRequest  = dbot.NewRequest("Upload data")
	requestRequest = dbot.NewRequest("Make a query")
	useVKRequest   = dbot.NewRequest("Use VK dataset")
	useGPNRequest  = dbot.NewRequest("Use GPN dataset")

	helpRequest = dbot.NewRequest("Help")

	setSendNewsRequest = dbot.NewRequest("Send news")

	backRequest         = dbot.NewRequest("\u2b05 Back")
	back2Request        = dbot.NewRequest("Back")
	unprescribedRequest = dbot.NewUnprescribedRequest()
)
