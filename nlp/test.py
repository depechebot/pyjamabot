import os
import sys

# FIXME: Test are outdated! It's a hackathon ¯\_(ツ)_/¯

tests = [
    (
        '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}',
        'What are the advertising costs for the last month?',
        'SELECT spent FROM table WHERE day >= "2018-02-11 07:53:56";'
    ),
    (
        '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}',
        'Какая стоимость рекламы за последний месяц?',
        'SELECT spent FROM table WHERE day >= "2018-02-11 07:53:56";'
    ),
    (
        '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}',
        'How much does it cost to click today?',
        'SELECT spent/clicks FROM table WHERE day >= "2018-03-11 07:53:58;'
    ),
    (
        '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}',
        'What is the average ctr for the last week?',
        'SELECT AVG(ctr) FROM table WHERE day >= "2018-03-04 07:54:09";'
    ),
    (
        '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}',
        'What is the average ctr from May 28 to June 10?',
        'SELECT AVG(ctr) FROM table WHERE day >= "2018-05-28 00:00:00" AND day <= "2018-06-10 00:00:00";'
    ),
    (
        '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}',
        'How much is the average cost to click per day?',
        'SELECT AVG(spent/clicks) FROM table WHERE day >= "2018-03-11 07:54:09";'
    ),
    (
        '{"day": "DATE", "impressions_rate": "FLOAT", "clicks_rate": "FLOAT", "ctr_rate": "FLOAT", "param_name": "ENUM", "city": "VARCHAR"}',
        'Which city has the highest ctr the day before yesterday?',
        'SELECT city FROM table WHERE day >= "2018-03-10 07:54:09" ORDER BY ctr_rate DESC LIMIT 1;',
    ),
    (
        '{"day": "DATE", "impressions_rate": "FLOAT", "clicks_rate": "FLOAT", "ctr_rate": "FLOAT", "param_name": "ENUM", "age": "INT"}',
        'At what age did you get more conversions for yesterday?',
        'SELECT age FROM table WHERE day >= "2018-03-10 07:54:09" ORDER BY ctr_rate DESC LIMIT 1;',
    ),
    (
        '{"day": "DATE", "impressions_rate": "FLOAT", "clicks_rate": "FLOAT", "ctr_rate": "FLOAT", "param_name": "ENUM", "sex": "VARCHAR"}',
        'What kind of gender clicks the most in a week?',
        'SELECT sex FROM table WHERE day >= "2018-03-03 07:54:09" ORDER BY clicks_rate DESC LIMIT 1;',
    )
]

for num, (schema, query, sql) in enumerate(tests):
    cmd = 'python3 parse.py \'{}\' \'{}\''.format(schema, query)
    print('TEST: #{}'.format(num + 1))
    print('SCHEMA:', schema)
    print('QUERY:', query)
    print('EXPECTED:', sql)
    print('RESULT:', end=' ')
    sys.stdout.flush()
    os.system(cmd)
    print('=======')
