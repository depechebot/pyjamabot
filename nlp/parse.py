import dateparser
import editdistance
import googletrans
import json
import spacy
import sqlite3
import sys

nlp = spacy.load('en')
translator = googletrans.Translator()


def detect_dates(doc):
    res = []
    for ent in doc.ents:
        if ent.label_ in ['DATE', 'TIME']:  # spacy types
            date = dateparser.parse(ent.text)
            text = ent.text
            res.append((ent.label_, text, date))
    return res


def detect_aggregation(query):
    words = [  # https://dev.mysql.com/doc/refman/5.7/en/group-by-functions.html
        ('AVG', ['avg', 'average', 'mean']),
        ('SUM', ['sum', 'total', 'amount', 'summa']),
        ('MAX', ['max', 'maximum']),
        ('MIN', ['min', 'minimum']),
        ('COUNT', ['number of', 'count']),
    ]
    for k, v in words:
        for word in v:
            if word in query:
                return k
    return ''


def detect_fields(schema, doc):

    #nouns = [token for token in doc if token.pos_ == 'NOUN' and not token.is_stop]
    nouns = [token for token in doc if not token.is_stop]
    # print(nouns)
    # for token in doc:
    #     print(token, token.pos_, token.is_stop)
    fields = [k for k, v in schema.items() if v in ['INT', 'INTEGER', 'FLOAT', 'DOUBLE']]  # sql types
    if not fields:  # if 0 number fields
        fields = [k for k, v in schema.items()]

    res = []

    field = fields[0] if fields else '*'
    best_dist = 1e10
    for word1 in nouns:
        for word2 in fields:
            dist = editdistance.eval(word1.text, word2) / max(len(word1.text), len(word2))
            if dist < best_dist:
                best_dist = dist
                field = word2

    res.append(field)

    return res

    # # Here is word2vec which works just awful :\
    # field = numfield[0] if numfield else '*'
    # best_similarity = 0
    # for token1 in nlp(query):
    #     for k in schema.keys():
    #         token2 = next(iter(nlp(k)))
    #         similarity = token1.similarity(token2)
    #         if similarity > best_similarity:
    #             best_similarity = similarity
    #             field = k


def parse(query, schema):

    doc = nlp(query)

    dates = detect_dates(doc)
    aggr = detect_aggregation(query)
    fields = detect_fields(schema, doc)

    where = ''
    datefilelds = [k for k, v in schema.items() if v in ['DATETIME', 'DATE', 'TIME']]  # sql types
    if datefilelds and dates and dates[0][2]:
        date = dates[0][2].strftime('%Y-%m-%d %H:%M:%S')
        #date = dates[0][2].strftime('%Y-%m-%d')
        where = ' WHERE {} >= "{}"'.format(datefilelds[0], date)

    field = fields[0]
    select = '{}({})'.format(aggr, field) if aggr else field

    sql = 'SELECT {} FROM t{};'.format(select, where)

    return sql


def run_query(csv_file, sql):
    # Dirty DB query
    # https://stackoverflow.com/a/2888042/92396
    con = sqlite3.connect(":memory:")
    cur = con.cursor()
    first = True
    header = None
    for line in open(csv_file, 'r'):
        line = line.strip().split(',')
        if first:
            header = line
            q = 'CREATE TABLE t ({});'.format(', '.join('{} {}'.format(item, schema[item]) for item in header))
            cur.execute(q)
            first = False
        else:
            q = 'INSERT INTO t ({}) VALUES ({});'.format(', '.join(header), ', '.join('"{}"'.format(x) for x in line))
            cur.execute(q)
    con.commit()
    res = []
    for row in cur.execute(sql):
        row = [str(round(x, 2)) if isinstance(x, float) else str(x) for x in row]
        res.append(' '.join(row))
    con.close()
    return '\n'.join(res)


if __name__ == '__main__':

    if len(sys.argv) < 3:
        print('Usage: python3', sys.argv[0], 'json_schema query csv_file')
        sample_query = 'What is the average cost to click for the last month?'
        sample_schema = '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}'
        sample_csv = '../preload_data/vk.csv'
        print('Example: python3', sys.argv[0], '\'{}\' \'{}\' \'{}\''.format(sample_schema, sample_query, sample_csv))
        exit()

    schema = json.loads(sys.argv[1])
    schema = dict((k.lower(), v) for k, v in schema.items())
    query = sys.argv[2]
    query = translator.translate(query, dest='en').text
    query = query.lower()
    sql = parse(query, schema)
    print(sql)

    csv_file = sys.argv[3]
    result = run_query(csv_file, sql)
    print(result)

