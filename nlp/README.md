SQL DB Schema + Human Query = SQL Query

# INSTALL

Requires `Python 3` (with `sqlite3`) and `pip`.

```
pip3 install dateparser
pip3 install spacy
pip3 install editdistance
pip3 install googletrans
python3 -m spacy download en
```

# USAGE

```
python3 parse.py json_schema query csv_file
```

Example:
```
python3 parse.py '{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}' 'What is the average cost to click for the last month?' '../preload_data/vk.csv'
```

# TEST

```
python3 test.py
```

