package main

import (
	"math/rand"
	"os/exec"
	"time"

	dbot "github.com/depechebot/depechebot"

	_ "github.com/mattn/go-sqlite3"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

var (
	startState = dbot.NewState("START")

	mainState    = dbot.NewState("MAIN")
	uploadState  = dbot.NewState("UPLOAD")
	requestState = dbot.NewState("REQUEST")

	hideKeyboardState = dbot.NewState("HIDE_KEYBOARD")
)

var statesConfigPrivate map[dbot.StateName]dbot.StateActions = map[dbot.StateName]dbot.StateActions{
	dbot.StartState.Name: dbot.StateActions{
		Before: nil,
		While:  dbot.StateWhile(),
		After:  dbot.StateAfter(dbot.Responsers{announceText, mainState}),
	},
	mainState.Name: dbot.StateActions{
		Before: dbot.StateBefore(chooseMenuText, [][]dbot.Request{
			{uploadRequest, requestRequest},
			{ /*settingsRequest,*/ helpRequest},
		}),
		While: dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			uploadRequest:  uploadState,
			requestRequest: requestState,
			/*settingsRequest:  settingsState,*/
			helpRequest: dbot.Responsers{helpText, mainState.SkippedBefore()},

			adminRequest:        adminState,
			unprescribedRequest: mainState,
		}),
	},
	uploadState.Name: dbot.StateActions{
		Before: dbot.StateBefore(uploadText, [][]dbot.Request{
			{useVKRequest},
			{useGPNRequest, backRequest},
		}),
		While: dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			/*uploadVKRequest:  dbot.Responsers{anyText, mainState.SkippedBefore()},*/
			unprescribedRequest: dbot.ResponseFunc(uploadAfterFunc),
			backRequest:         mainState,
		}),
	},
	requestState.Name: dbot.StateActions{
		Before: dbot.StateBefore(requestText, [][]dbot.Request{
			{usePreloadRequest},
			{backRequest},
		}),
		While: dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			usePreloadRequest:   dbot.ResponseFunc(requestAfterFunc),
			unprescribedRequest: dbot.ResponseFunc(requestAfterFunc),
			backRequest:         mainState,
		}),
	},
}

//////////////////////////////////////////////////////////////////
func uploadAfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {
	text := dbot.NewText("Thank you! We've received your data.")
	text.Response(bot, chat, update, state, params)
	*state = mainState
}

func requestAfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {
	//text := dbot.NewText("Your query is being analyzed...")
	request := update.Message.Text

	if update.Message.Text == usePreloadRequest.Text {
		rand.Seed(time.Now().UTC().UnixNano())
		//rand.Seed(time.Now())
		request = VKQuestions[rand.Intn(len(VKQuestions))]
	}
	answer, err := getAnswer(request)
	if err != nil {
		//text
	}

	text := dbot.NewTextWithMarkdown(answer)
	text.Response(bot, chat, update, state, params)
	*state = requestState
}

const (
	basePath = "/home/fun/go/src/bitbucket.org/depechebot/pyjamabot/"
)

func getAnswer(request string) (string, error) {
	answer := "Your request: *" + request + "*\n"

	out, err := exec.Command(
		"python3",
		basePath+"nlp/parse.py",
		VKHeader,
		request,
		"../preload_data/t.csv",
	).Output()

	if err != nil {
		return "Ошибка", err
	}

	sql := string(out)
	answer = answer + "You SQL request: `" + sql + "`\n"

	/*
		out, err = exec.Command(
			"textql",
			"-header",
			"-sql",
			sql,
			basePath+"preload_data/t.csv",
		).Output()

		if err != nil {
			//return "Error", err
			return "Error", nil
		}

		result := string(out)
		answer = answer + "Answer: *" + result + "*\n"
	*/

	return answer, err
}

/*
func sched2AfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {
	sched, err := getSched(update.Message.Text)
	if err != nil {
		text := dbot.NewText("Error! Please contact system administrator.")
		text.Response(bot, chat, update, state, params)
		log.Println("Error while retrieving schedule")
		*state = infoState
	} else {
		schedText := dbot.NewTextWithMarkdown(sched)
		schedText.Response(bot, chat, update, state, params)
		*state = schedState.SkippedBefore()
	}
}

func aboutMeAfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {
	msg := tgbotapi.NewMessage(int64(chat.ChatID), answerOkText.Text)
	bot.SendChan <- dbot.ChatSignal{msg, dbot.ChatID(chat.ChatID)}

	if len(update.Message.Text) > 10 {
		params.Set("aboutMe", update.Message.Text)
	}
	*state = mainState
}

func aboutOthersAfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {

	var err error
	results, err := bot.Config.Model.ChatsByParam("aboutMe")
	if err != nil {
		log.Println("DB issue: ", err)
		*state = mainState
		return
	}

	for _, res := range results {

		var name string

		aboutMe := res.Params["aboutMe"]

		if res.UserName != "" {
			name = fmt.Sprintf("%s %s (@%s)", res.FirstName, res.LastName, res.UserName)
		} else {
			name = fmt.Sprintf("%s %s", res.FirstName, res.LastName)
		}

		text := fmt.Sprintf("%s\n%s", name, aboutMe)
		msg := tgbotapi.NewMessage(int64(chat.ChatID), text)
		msg.DisableWebPagePreview = true
		bot.SendChan <- dbot.ChatSignal{msg, chat.ChatID}
	}

	*state = mainState.SkippedBefore()
}
*/
/////////////////////////////////////////////////////////////////

var statesConfigGroup map[dbot.StateName]dbot.StateActions = map[dbot.StateName]dbot.StateActions{
	dbot.StartState.Name: dbot.StateActions{
		Before: nil,
		While:  nil,
		After:  dbot.StateAfter(mainState),
	},
	mainState.Name: dbot.StateActions{
		Before: nil,
		While:  dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			//programRequest:      programText,
			unprescribedRequest: mainState,
		}),
	},
}

///////////////////////////////////////////////////////////
/*
var sayResponseFunc dbot.ResponseFunc = func(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {

	text := update.Message.Text
	msg := tgbotapi.NewMessage(groupChatID, text)
	bot.SendChan <- dbot.ChatSignal{msg, groupChatID}

	msg = tgbotapi.NewMessage(int64(chat.ChatID), sayOkText.Text)
	bot.SendChan <- dbot.ChatSignal{msg, chat.ChatID}

	*state = hideKeyboardState
}
*/
