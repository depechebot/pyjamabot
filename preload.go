package main

const (
	VKHeader = `{"day": "DATE", "spent": "FLOAT", "impressions": "INT", "clicks": "INT", "reach": "INT", "join_rate": "INT", "ctr": "FLOAT"}`
)

var (
	VKQuestions = []string{
		"What are the total advertising spents for the last month?",
		"How much does it cost to click yesterday?",
		//"On what day was advertising the most effective?",
		"What is the average ctr from January 28 10:00?",
		"What is the average ctr for the last week?",
		"How much is the average spent since yesterday?",
		"Какая средняя стоимость клика с 1 февраля?",
		"¿Cuál es el costo promedio por clic desde el 3 de febrero?",
		"自2月1日以来每次点击的平均费用是多少？",
		//"Which city has the highest ctr the day before yesterday?",
		//"At what age did you get more conversions for yesterday?",
		//"What kind of gender clicks the most in a week?",
	}
)
