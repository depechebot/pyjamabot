
drop table chat2;
CREATE TABLE chat2 (
  primary_id INTEGER NOT NULL PRIMARY KEY,
  chat_id BIGINT UNIQUE NOT NULL,
  type TEXT NOT NULL,
  abandoned INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  user_name TEXT NOT NULL DEFAULT '',
  real_name TEXT NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  open_time timestamptz NOT NULL,
  last_time DATETIME NOT NULL,
  groups TEXT NOT NULL,
  state TEXT NOT NULL
);


insert into chat2
(primary_id, chat_id, type, abandoned, user_id, user_name, real_name, first_name, last_name, open_time, last_time, groups, state)
select
primary_id, chat_id, 'private', 0, user_id, username, realname, first_name, last_name, open_date, last_message_date, 
'{"phone":"' || phone || '", "social": "' || social || '", "email": "' || email || '", ' || substr(groups, 2),
'{"name":"MAIN","parameters":"{}"}'
from Chat;

