package json

import (
	"encoding/json"
	"io/ioutil"

	"bitbucket.org/depechebot/stat"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

type Model struct {
	s        *stat.Stat
	filename string
}

func NewModel(filename string) Model {
	s := stat.NewStat()
	return Model{filename: filename, s: &s}
}

func (m *Model) Init() error {
	err := m.load()
	if err != nil {
		return err
	}

	if m.s == nil {
		s := stat.NewStat()
		m.s = &s
	}

	return nil
}

func (m Model) save() error {

	data, err := json.Marshal(m.s)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(m.filename, data, 0644)
}

func (m Model) GetStat() (*stat.Stat, error) {

	//	err := m.load()
	//	if err != nil {
	//		return nil, err
	//	}

	return m.s, nil
}

func (m Model) load() error {
	data, err := ioutil.ReadFile(m.filename)

	if err != nil {
		s := stat.NewStat()
		m.s = &s

		return nil
	}
	err = json.Unmarshal([]byte(data), m.s)

	return err
}

func (m Model) Update(u *tgbotapi.Update) error {
	// todo: should be atomic
	m.s.Update(u)
	return m.save()
}
