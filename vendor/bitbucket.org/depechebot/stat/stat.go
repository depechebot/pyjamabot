package stat

import tgbotapi "gopkg.in/telegram-bot-api.v4"

type Stat struct {
	UsersNum         int            `json:"users_num"`
	UpdatesNum       int            `json:"updates_num"`
	UpdatesHistogram map[string]int `json:"updates_histogram"`
}

type Model interface {
	Init() error
	GetStat() (*Stat, error)
	Update(u *tgbotapi.Update) error
}

func NewStat() Stat {
	return Stat{UpdatesHistogram: make(map[string]int)}
}

func (s *Stat) Update(u *tgbotapi.Update) {
	s.UpdatesNum += 1
	if u.Message.Text != "" {
		s.UpdatesHistogram[u.Message.Text] += 1
	}
}
