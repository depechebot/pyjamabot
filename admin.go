package main

import (
	"bufio"
	"fmt"
	"log"
	"strings"

	dbot "github.com/depechebot/depechebot"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

var (
	adminState         = dbot.NewState("ADMIN")
	adminStatState     = dbot.NewState("ADMIN_STAT")
	adminSendState     = dbot.NewState("ADMIN_SEND")
	adminSendWhatState = dbot.NewState("ADMIN_SEND_WHAT")
)

const (
	adminPass = "pyjamaadmin"
)

var (
	askSendThisText = dbot.NewText("Отослать всем сообщение?")
	sentToAllText   = dbot.NewText("Сообщение всем отправлено!")

	adminLogText = dbot.NewText("Нажмите на ссылку для добавления в чат истории. Вы можете в любой момент выйти из этого чата и присоединиться снова. Советуем отключить уведомления (mute notifications) для этого чата, чтобы не отвлекаться на пользовательские запросы.\n\n https://t.me/joinchat/BLXsyk-3k6vHyHkqk2lguw")

	adminSendWhatText  = dbot.NewText("Наберите сообщение для рассылки:")
	adminSendWhereText = dbot.NewText("Выберите рассылку:")
	adminSentText      = dbot.NewText("Сообщение отослано!")
)

var (
	adminRequest         = dbot.NewRequest(adminPass)
	adminLogRequest      = dbot.NewRequest("История")
	adminStatRequest     = dbot.NewRequest("Статистика")
	adminUsersRequest    = dbot.NewRequest("Пользователи")
	adminRequestsRequest = dbot.NewRequest("Запросы")
	adminSendRequest     = dbot.NewRequest("Рассылка")
	adminSendAllRequest  = dbot.NewRequest("Всем")
)

var (
	adminLogParam = dbot.NewParams("adminLog", "yes")
)

var adminStatesConfigPrivate map[dbot.StateName]dbot.StateActions = map[dbot.StateName]dbot.StateActions{

	adminState.Name: dbot.StateActions{
		Before: dbot.StateBefore(chooseMenuText, [][]dbot.Request{
			{adminSendRequest, adminStatRequest},
			{backRequest}}),
		While: dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			adminSendRequest: adminSendState,
			adminStatRequest: adminStatState,

			backRequest:         mainState,
			unprescribedRequest: mainState,
		}),
	},
	adminStatState.Name: dbot.StateActions{
		Before: dbot.StateBefore(chooseMenuText, [][]dbot.Request{
			{adminUsersRequest, adminRequestsRequest},
			{adminLogRequest, backRequest},
		}),
		While: dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			adminLogRequest:      dbot.Responsers{adminLogText, adminLogParam, adminStatState.SkippedBefore()},
			adminUsersRequest:    dbot.ResponseFunc(adminUsersAfterFunc),
			adminRequestsRequest: dbot.ResponseFunc(adminRequestsAfterFunc),

			backRequest:         adminState,
			unprescribedRequest: mainState,
		}),
	},
	adminSendState.Name: dbot.StateActions{
		Before: dbot.StateBefore(adminSendWhereText, [][]dbot.Request{
			{adminSendAllRequest, backRequest},
		}),
		While: dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			//adminSendAllRequest:    sendToAllAfterFunc,
			adminSendAllRequest: adminSendWhatState.WithParam("recipient", "all"),

			backRequest:         adminState,
			unprescribedRequest: mainState,
		}),
	},
	adminSendWhatState.Name: dbot.StateActions{
		Before: dbot.StateBefore(adminSendWhatText, []dbot.Request{backRequest}),
		While:  dbot.StateWhile(),
		After: dbot.StateAfter(dbot.ReqToRes{
			backRequest:         adminSendState,
			unprescribedRequest: getSendToAfterFuncByRecipientType("potok"),
		}),
	},
}

func adminUsersAfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {

	var err error
	results, err := bot.Config.Model.ChatsByParam("{") // all
	if err != nil {
		log.Println("DB issue: ", err)
		*state = adminState
		return
	}
	str2 := ""
	for _, res := range results {
		if res.UserName != "" {
			str2 += fmt.Sprintf("%s %s (@%s)\n", res.FirstName, res.LastName, res.UserName)
		} else {
			str2 += fmt.Sprintf("%s %s\n", res.FirstName, res.LastName)
		}
	}
	text2 := dbot.NewText(str2)
	splitText(text2).Response(bot, chat, update, state, params)

	str := fmt.Sprintf("Число пользователей: %d", len(results))
	text := dbot.NewText(str)
	text.Response(bot, chat, update, state, params)

	*state = adminStatState.SkippedBefore()
}

func splitText(t dbot.Text) dbot.Responsers {
	const maxLen = 4000

	if len(t.Text) < maxLen {
		return dbot.Responsers{dbot.Responser(t)}
	}

	scanner := bufio.NewScanner(strings.NewReader(t.Text))
	var texts []dbot.Responser
	sum := 0
	text := ""
	for scanner.Scan() {
		line := scanner.Text()
		if len(text)+len(line)+1 >= maxLen {
			texts = append(texts, dbot.Responser(dbot.NewText(text)))
			sum = 0
			text = ""
		} else {
			sum += len(line) + 1
			text += line + "\n"
		}
	}
	if sum != 0 {
		texts = append(texts, dbot.Responser(dbot.NewText(text)))
	}
	return texts
}

func adminRequestsAfterFunc(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {
	stat, _ := statModel.GetStat()
	hist := stat.GetSortedUpdatesHistogram()

	str1 := fmt.Sprintf("Число запросов к боту: %d", stat.UpdatesNum)
	str2 := ""
	for _, pair := range hist {
		str2 += fmt.Sprintf("%d \"%s\"\n", pair.Value, pair.Key)
	}

	text2 := dbot.NewText(str2)
	splitText(text2).Response(bot, chat, update, state, params)

	text1 := dbot.NewText(str1)
	text1.Response(bot, chat, update, state, params)

	*state = adminStatState.SkippedBefore()
}

func getSendToAfterFuncByRecipientType(recipientType string) dbot.ResponseFunc {
	return func(bot dbot.Bot, chat dbot.Chat, update tgbotapi.Update, state *dbot.State, params *dbot.Params) {
		var searchString string

		recipient := state.Params.Get("recipient")
		if recipient == "all" {
			searchString = fmt.Sprintf(`"%s":"%s"`, "reg", "Ok")
		} else {
			recipient = state.Params.Get(recipientType)
			searchString = fmt.Sprintf(`"%s":"%s"`, recipientType, recipient)
		}

		var err error
		results, err := bot.Config.Model.ChatsByParam(searchString)
		if err != nil {
			log.Println("DB issue: ", err)
			*state = adminState
			return
		}

		var list []dbot.ChatID
		for _, res := range results {
			list = append(list, res.ChatID)
		}

		if update.Message.Text != "" {
			msg := tgbotapi.NewMessage(int64(chat.ChatID), update.Message.Text)
			bot.SendBroadChan <- dbot.BroadSignal{msg, list}
		} else if update.Message.Document != nil {
			msg := tgbotapi.NewDocumentShare(int64(chat.ChatID), update.Message.Document.FileID)
			bot.SendBroadChan <- dbot.BroadSignal{msg, list}
		} else if update.Message.Photo != nil {
			msg := tgbotapi.NewPhotoShare(int64(chat.ChatID), (*update.Message.Photo)[len(*update.Message.Photo)-1].FileID)
			bot.SendBroadChan <- dbot.BroadSignal{msg, list}
		} else if update.Message.Audio != nil {
			msg := tgbotapi.NewAudioShare(int64(chat.ChatID), update.Message.Audio.FileID)
			bot.SendBroadChan <- dbot.BroadSignal{msg, list}
		}

		//msg := tgbotapi.NewMessage(int64(chat.ChatID), marshal(list))
		//bot.SendChan <- dbot.ChatSignal{msg, chat.ChatID}

		msg := tgbotapi.NewMessage(int64(chat.ChatID), adminSentText.Text)
		bot.SendChan <- dbot.ChatSignal{msg, chat.ChatID}

		*state = adminState
	}
}
